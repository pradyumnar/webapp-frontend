function isApplianceOrMixer(name) {
    return (name.includes("appliance") || name.includes("mixer"))
}

function isDurableLongLasting(item) {
    return isApplianceOrMixer(item.name) && item.price > 1500
}

function isPremiumItem(item) {
    return item.price > 1000
}

function isLongDescriptionItem(item) {
    return item.description.length > 30
}

// TODO: Extract item description updation into method
function updateItem(item) {
    if (isPremiumItem(item)) {
        item.name = "Premium " + item.name
    }

    if (isDurableLongLasting(item)) {
        item.description = "Durable long lasting world class " + item.name
    }

    if (isLongDescriptionItem(item)) {
        item.description = item.description + `. More information available on YouTube channel ${item.name}`
    }

    return item
}

module.exports = updateItem;
