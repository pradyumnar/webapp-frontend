const updateItem = require('./updateItem');

test('If price > 1000 then name "Premium <name>"', () => {
    var item = {
        id: 11,
        name: "Pen",
        price: 1200,
        description: "A cool pen"
    }

    var updatedItem = updateItem(item)

    expect(updatedItem.name).toBe("Premium Pen");
});

test('Name contains "appliance" and price > 1500 then description "Durable long lasting world class <name>"', () => {
    var item = {
        id: 11,
        name: "Pen appliance",
        price: 1600,
        description: "A cool pen"
    }

    var updatedItem = updateItem(item)

    expect(updatedItem.description).toBe("Durable long lasting world class Premium Pen appliance. More information available on YouTube channel Premium Pen appliance");
});

test('Name contains "mixer" and price > 1500 then description "Durable long lasting world class <name>"', () => {
    var item = {
        id: 11,
        name: "Pen mixer",
        price: 1501,
        description: "A cool pen"
    }

    var updatedItem = updateItem(item)

    expect(updatedItem.description).toBe("Durable long lasting world class Premium Pen mixer. More information available on YouTube channel Premium Pen mixer");
});

test('Description > 30 characters, then should append ". More information available on YouTube channel <name>"', () => {
    var item = {
        id: 11,
        name: "Pen",
        price: 801,
        description: "This is a really long description that I thought of when making the pen"
    }

    var updatedItem = updateItem(item)

    expect(updatedItem.description).toBe("This is a really long description that I thought of when making the pen. More information available on YouTube channel Pen");
});


