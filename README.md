# Webapp Frontend Javascript

## Stories
- If price is more than 1000 then name should be "Premium \<name>"
- If name contains "appliance" or "mixer" and price > 1500 then description is "Durable long lasting world class \<name>"
- If the description is longer than 30 characters, the description should be appended with ". More information available on YouTube channel \<name>"
